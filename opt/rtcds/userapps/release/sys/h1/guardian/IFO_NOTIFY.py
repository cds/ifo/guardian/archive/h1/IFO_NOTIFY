"""This node will monitor the IFO and decide when to notify
the on-call staff member. This works in conjunction with Daves
notification software by setting an epics channel to 1 if 
the notification system should send out alerts, and 0 if all 
is well.
"""

from guardian import GuardState, NodeManager
import time
from IFO_NODE_LIST import get_nominal_nodes
from static_tester import StaticTester
# Using the time module here rather than calling the usual H1:DAQ-DC0_GPS
# for the time, to attempt to remove any dependencies on other machines.

nominal = 'ALERTS_ENABLED'
#############################################
# Nodes to monitor
# We will only put them in here to watch their state,
# not to actually manage them. 

nodes = NodeManager(get_nominal_nodes() + ['IFO', 'H1_MANAGER'])
#############################################
# Settings

nln_statenum = 600
snews_wait_time = 1800

# Set of times to wait after an event before alerting
### IN SECONDS!!!
laser_fudge = 1
not_at_ready = 15 * 60 * laser_fudge
#green_not_locked = 35 * 60
#drmi_not_locked = 35 * 60
default_wait = (90 + 30) * 60 * laser_fudge
nln_not_obs = 8 * 60 * laser_fudge
conner = 10 * 60 * laser_fudge



#############################################
# Const
hams = ['HAM2', 'HAM3', 'HAM4', 'HAM5', 'HAM6']
chambers = ['BS', 'ITMX', 'ITMY', 'ETMX', 'ETMY']
extra = ['TMSY', 'TMSX', 'OMC', 'MC1', 'MC2', 'MC3', 'SRM', 'SR2', 'SR3', 'PRM', 'PR2', 'PR3']
seis = hams + chambers
sus = chambers + extra
total = {'SEI':seis,'SUS':sus}

quads = ['ETMX','ETMY','ITMX','ITMY']
suspensions = {
    'BS': ['M1','M2'],
    'ITMX': ['M0','R0','L1','L2'],
    'ITMY': ['M0','R0','L1','L2'],
    'ETMX': ['M0','R0','L1','L2'],
    'ETMY': ['M0','R0','L1','L2'],
    'PRM': ['M1','M2','M3'],
    'PR2': ['M1','M2','M3'],
    'PR3': ['M1','M2','M3'],
    'MC1': ['M1','M2','M3'],
    'MC2': ['M1','M2','M3'],
    'MC3': ['M1','M2','M3'],
    'SRM': ['M1','M2','M3'],
    'SR2': ['M1','M2','M3'],
    'SR3': ['M1','M2','M3'],
    'OMC': ['M1'],
    'TMSX': ['M1'],
    'TMSY': ['M1'],
    'RM1': ['M1'],
    'RM2': ['M1'],
    'IM1': ['M1'],
    'IM2': ['M1'],
    'IM3': ['M1'],
    'IM4': ['M1'],
    'OM1': ['M1'],
    'OM2': ['M1'],
    'OM3': ['M1'],
    'OPO': ['M1'],
    'OFI': ['M1'],
    'ZM1': ['M1'],
    'ZM2': ['M1'],
}

wd_chans = []
for i in seis + ['HAM1']:
    wd_chans.append('HPI-{}_WD_MON_STATE_INMON'.format(i))
for i in chambers:
    for num in ['1', '2']:
        wd_chans.append('ISI-{}_ST{}_WD_MON_STATE_INMON'.format(i, num))
for ham in hams:
    wd_chans.append('ISI-{}_WD_MON_STATE_INMON'.format(ham))
for sus, level in suspensions.items():
    for lev in level:
        wd_chans.append('SUS-{}_{}_WDMON_STATE'.format(sus, lev))


# For SQZ causing the range to drop
sqz_chan = 'SQZ-OPO_REFL_DC_POWER'
sqz_st = StaticTester([sqz_chan], static_time=60, greater_than=1.15)

#############################################
# States

class INIT(GuardState):
    """

    """
    def run(self):
        notify('Please select the operating state for the node')
        return True

class ALERTS_ENABLED(GuardState):
    """This is the state that will be requested, but never reached.
    It is just for semantics and making it more clear as to what 
    this node is doing.
    """
    index = 40
    def run(self):
        return True

        
'''
class SNEWS_ALERT_ACTIVE(GuardState):
    """Jump here if there is a SNEWS alert.
    the LLA system will them make a phone call to 
    the operator.
    """
    index = 35
    request = False
    redirect = False
    def run(self):
        if ezca['CAL-INJ_EXTTRIG_ALERT_SOURCE'].upper() == 'SNEWS' \
           and ezca['DAQ-DC0_GPS'] - ezca['CAL-INJ_EXTTRIG_ALERT_TIME'] < snews_wait_time:
            return False
        else:
            # Jump to WAITING so we can start this all over again
            return 'WAITING'
'''

class ALERT_ACTIVE(GuardState):
    """A nominal requestable state.

    """
    index = 30
    request = False
    def run(self):
        if nodes['IFO'].state == 'OBSERVE':
            return 'WAITING'
        else:
            notify('See log for alert. Select INIT to reset')
            return False



class WAITING(GuardState):
    """Wait for a condition to alert.

    """
    index = 20
    request = False
    goto = True
    def main(self):
        # Set up a dictionary to hold of the times and attempts
        # Setting these to some absurd number as a place holder
        self.records = {'lockloss': 1e20,
                        'not_ready': 1e20,
                        #'green_start': 1e20,
                        #'drmi_start': 1e20,
                        'nln_start': 1e20,
        }
        # No way to get the number from the manager
        self.isc_lock_prev_n = ezca['GRD-ISC_LOCK_STATE_N']
        self.ifo_prev = nodes['IFO'].state

    def run(self):
        # Mark time
        time_now = time.time()
        # Get new states
        isc_lock_now_n = ezca['GRD-ISC_LOCK_STATE_N']
        ifo_now = nodes['IFO'].state

        ### Lock loss
        if isc_lock_now_n != self.isc_lock_prev_n:
            # We have it just looking for if state now is below nln because it can miss
            # the LOCKLOSS state since it is fast.
            if self.isc_lock_prev_n >= nln_statenum and isc_lock_now_n < nln_statenum:
                # Mark the time of the start of the lock reacq
                self.records['lockloss'] = time.time()

        if nodes['H1_MANAGER'] == 'ASSISTANCE_REQ':
            log('H1_MANAGER requires assistance. Sending')
            return True

        ##### Observing
        if nodes['IFO'].state == 'OBSERVE':
            # Reset all records
            for ent in self.records:
                self.records[ent] = 1e20
            # And vals
            self.isc_lock_prev_n = isc_lock_now_n
            self.ifo_prev = ifo_now
            return

        ##### Out of Observing tests
        
        ### GRD code error
        for node in nodes:
            if node.ERROR:
                log('GRD node {} is in error. Sending'.format(node.name))
                return True
            
        ### WDs
        for chan in wd_chans:
            if ezca[chan] != 1:
                log('WD Tripped ({}). Sending'.format(chan))
                return True
        
        ### Not getting past ready
        # Again its odd because we can miss the LOCKLOSS state
        if self.isc_lock_prev_n != 10 \
           and isc_lock_now_n == 10:
            log('Setting \'not_ready\' = {}'.format(time_now))
            self.records['not_ready'] = time_now
            # Reset the nln_start timer since it might get missed if we went dropped observe first
            self.records['nln_start'] = 1e20
        elif (isc_lock_now_n == 10 or isc_lock_now_n == 9) \
             and time_now - self.records['not_ready'] > not_at_ready:
            log('Not past READY for {}min. Sending'.format(not_at_ready/60))
            return True

        ### NLN not Obs
        elif isc_lock_now_n == nln_statenum and nodes['IFO'].state != 'OBSERVE':
            # If dropped OBSERVE
            if self.ifo_prev == 'OBSERVE':
                self.records['nln_start'] = time_now
            # If just reached NLN
            elif self.isc_lock_prev_n < nln_statenum:
                self.records['nln_start'] = time_now
            # If nln timer expires
            elif time_now - self.records['nln_start'] > nln_not_obs:
                lt = time.localtime()
                # If it is before Tuesday maintenance, dont alert for PEM/SUS inj
                # tm_wday starts at 0=Monday
                if lt.tm_wday == 1 and lt.tm_hour == 7 and lt.tm_min >= 20:
                    return False
                else:
                    log('In NLN for {}min, check SDFs. Sending'.format(nln_not_obs/60))
                    return True

        ##### Update vals
        self.isc_lock_prev_n = isc_lock_now_n
        self.ifo_prev = ifo_now
        
        return False
        '''
        ### DRMI not locked by timeout
        # DRMI start
        elif self.isc_lock_prev_n == 18 \
             and 103 > isc_lock_now_n > 18:
            self.records['drmi_start'] = time_now
        # DRMI timeout
        elif 103 > isc_lock_now_n > 18 \
             and time_now - self.records['drmi_start'] > drmi_not_locked:
            log('DRMI not locked in {}min. Sending'.format(drmi_not_locked/60))
            return True

        ### Default timeout
        elif isc_lock_now_n < nln_statenum \
             and time_now - self.records['lockloss'] > default_wait:
            log('Default timeout ({}min). Sending'.format(default_wait/60))
            return True
        '''


class IDLE(GuardState):
    """State for us to park it when we dont need it.
    
    """
    index = 10
    goto = True
    def run(self):
        return True
    

#############################################
# Edges

edges = [('IDLE', 'WAITING'),
         ('WAITING', 'ALERT_ACTIVE'),
         ('ALERT_ACTIVE', 'ALERTS_ENABLED'),
]
